[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![pipeline](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/badges/master/pipeline.svg)](https://gitlab.esss.lu.se/ics-infrastructure/pynessus/pipelines)
[![coverage](https://gitlab.esss.lu.se/ics-infrastructure/ess-pynotify/badges/master/coverage.svg)](https://gitlab.esss.lu.se/ics-infrastructure/pynessus/pipelines)

## Python-nessus
pynessus is a **command line utility** written in *python*, which provides the user facility to *automate vulnerability scanning using Nessus REST API*.

Depending on the flag issued, it can *list all policies*, *create and launch the scan*, *configure the user-defined policy prior to launching the scan* and *export the report in all available formats* except for pdf. The command line utility has the *power* to do all this just by entering a single command.

## Pre-Requisites
```
python3.x
Nessus 6 or above
pip for python3.x
```
## Installation
Ideally, you should be able to just type:
```bash
git clone https://gitlab.esss.lu.se/ics-infrastructure/pynessus.git
cd pynessus
pip3 install . # doing this in a virtual environment is better
```
## Preparations
*pynessus* requires a username, password and Nessus (Manager, Scanner, IO) https address.
These should be set  as environment variables as shown below.

Set the following environment variables as follows:

>`NESSUS_USER=readonly`
>`NESSUS_PASS=readonlapassy`
>`NESSUS_HOST=https://nessushost.domain:8834`

## How to use
To list all policies :

>`pynessus --list-policies`

To create and launch a scan

>`pynessus -t target_ip -p "policy_name"`

To create and launch a scan taking targets from a text file

>`pynessus -T target_file -p "policy_name"`

To launch the scan with a customized name and export the report in csv format

>`pynessus -T target_file -p "policy_name" -n scan_name -e csv`

To delete the scan after exporting the report

>`pynessus -T target_file -p "policy_name" -e html --delete`

To configure the User-defined policy prior to launching the scan

>`pynessus -t target_ip -p "policy_name" --configure -e nessus`

## Export Report formats
```
___________________________________________________________________________
| csv    | A comma-separated values (CSV)                                  |
| db     | A proprietary encrypted database format                         |
| html   | A report generated using standard HTML output                   |
| nessus | A .nessus file in XML format that contains the list of targets  |
|__________________________________________________________________________|
```
## How to uninstall
```bash
pip3 uninstall pynessus
```

## Original Project
Admiral Gaust https://github.com/AdmiralGaust/python-nessus

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open("README.md") as f:
        return f.read()


setup(
    name="pynessus",
    version="0.0.1",
    description="pynessus is a cli tool to automate vulnerability scanning using Nessus REST API.",
    long_description=readme(),
    url="https://gitlab.esss.lu.se/ics-infrastructure/pynessus",
    author="Remy Mudingay",
    author_email="remy.mudingay@ess.eu",
    license="MIT",
    scripts=["bin/pynessus"],
    install_requires=["argparse", "requests", "termcolor"],
    test_suite="tests",
    include_package_data=True,
    zip_safe=False,
)
